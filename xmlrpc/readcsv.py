import openerplib

class Products():
    def read_using_csv(self, connection):
        product_model = connection.get_model('product.product')
        
        product_ids = product_model.search([('name', 'ilike', 'ipad')])
        
        products = product_model.export_data(product_ids, ['id', 'name', 'list_price'])
        for product in products['datas']:
            print product

if __name__ == '__main__':
    #Connect by xml-rpc
    connection = openerplib.get_connection(hostname="localhost", 
                                           port=8069, 
                                           database="xp2015", 
                                           login="admin", 
                                           password="admin", 
                                           protocol="jsonrpc",
                                           user_id=1)
                                           
    connection.check_login()
            
    prod = Products()
    prod.read_using_csv(connection)
