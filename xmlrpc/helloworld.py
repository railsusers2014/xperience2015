import openerplib

class HelloWorld():
    def say_hello(self, connection):
        partner_model = connection.get_model('res.partner')
        
        partner_ids = partner_model.search([('name', 'ilike', 'fletcher')])
        partners = partner_model.read(partner_ids, ['name', 'parent_id'])
        for partner in partners:
            print partner
            res = "Hello %s" % partner['name']
            if partner['parent_id']:
                res = "%s from %s" % (res, partner['parent_id'][1])
            print res

if __name__ == '__main__':
    #Connect by xml-rpc
    connection = openerplib.get_connection(hostname="localhost", 
                                           port=8069, 
                                           database="xp2015", 
                                           login="admin", 
                                           password="admin", 
                                           protocol="jsonrpc",
                                           user_id=1)
                                           
    connection.check_login()
            
    imp = HelloWorld()
    imp.say_hello(connection)
    
