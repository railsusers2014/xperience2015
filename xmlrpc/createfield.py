import openerplib

class Partner():
    def add_field(self, connection):
        model_model = connection.get_model('ir.model')
        field_model = connection.get_model('ir.model.fields')
        
        model_id = model_model.search([('model', '=', 'res.partner')])
        field_id = field_model.create({
            'name': 'x_demo',
            'field_description': 'Demo field',
            'model': 'res.partner',
            'model_id': model_id[0],
            'ttype': 'char',
            'state': 'manual',
        })

        partner_model = connection.get_model('res.partner')
        partner_ids = partner_model.search([('name', 'ilike', 'fletcher')])
        partner_model.write(partner_ids, {'x_demo': 'xperience 2015'})
        partners = partner_model.read(partner_ids, ['x_demo', 'name'])
        for partner in partners:
            print '%s : %s' % (partner['name'], partner['x_demo'])

if __name__ == '__main__':
    #Connect by xml-rpc
    connection = openerplib.get_connection(hostname="localhost", 
                                           port=8069, 
                                           database="xp2015", 
                                           login="admin", 
                                           password="admin", 
                                           protocol="jsonrpc",
                                           user_id=1)
                                           
    connection.check_login()
    
    prtn = Partner()
    prtn.add_field(connection)
